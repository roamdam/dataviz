Data visualisations
-------------------

_This repository contains codes and data for different data visualisation
experiments and training._

# Plots from an hourly distribution with R and ggplot2

This exercice was part of [Udacity Data Vizualisation and D3.js course](https://www.udacity.com/course/data-visualization-and-d3js--ud507). 
Both data visualisations were inspired by the [depecheplot](https://pbs.twimg.com/media/DExYd95XkAA5uwR.jpg)
and made with R (Rstudio) and ggplot2.
Cleaning script and ideas courtesy of [@hnrklndbrg](https://twitter.com/hnrklndbrg/status/886181647003119616).

The repo contains files for creating the plots from scratch :
* `clean-data.R` cleans the raw data from `data/activities_raw.tsv` to produce 
  the datasets needed for the plot
* `clock-plot.R` and `voice-plot.R` create the data visualisation as images saved
  in folder `outs`.

## The _clock plot_

The clock plot is an experiment on using polar coordinates in an unusual way to
represent distributions. It may be viewed as a circle-wise distribution histogram.

![clock-plot](outs/clockplot.svg)

Apart from the aesthetic objective to mimic a plot representing hour data, it
still allows to identify peaks and hollows in the distribution, and compare peak
time and global distribution from one activity to an other.

## The _voice plot_

That kind of plot is not so much different from a _joy plot_, except that it
allows better quantification. It is sort of a simple distribution histogram, but
with an horizontal symetry for aesthetic matters, which gives it its dictaphone
look.

![voice-plot](outs/voiceplot.svg)

In a snapshot, it allows the viewer to identify which activities are concentrated
around peaktime (median is close to the red bars) and which have several peak
times, usually one in the morning and an other in the afternoon (median is in the
middle gray-yellow area).

# Exercices from stack overflow

## The moran plot

This is an exercice from [this SO question](https://stackoverflow.com/questions/59600252/r-ploting-moran-i-plot-in-ggplot/59600713#59600713),
to try and convert a base R *moran plot* from the `spdep` package into a ggplot2 plot.
I'm not a big fan of the background grid but I let it to give it its ggplot look.

![moran-plot](outs/moran-plot.png)

Full code available in `moran-plot.R` script in this repo. Feel free to use and customise to your needs.
